if (!angular) {
  throw 'angular package is expected in your project! Load it first.'
}
else {
  angular.module('ldrc.avatar', []);
}