Package.describe({
  name: 'aldricus:avatar-angular',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'AngularJs wrapper for Meteor\'s aldricus:avatar package',
  // URL to the Git repository containing the source code for this package.
  git: 'https://aldricus@bitbucket.org/aldricus/ldrc-avatar-angular.git',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.0.1');
  api.use('angular:angular@1.3.2', 'client');
  api.use('blaze-html-templates', 'client');

  api.imply('aldricus:avatar');

  api.addFiles([
    'avatar-angular.js',
    'avatar-directive.js'
  ], 'client');
});
