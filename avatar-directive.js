angular.module('ldrc.avatar').directive('ldrcAvatar', function() {
	return {
		restrict: 'E',
		scope: true,
		link: function(scope, element, attrs) {
			var data = {};
			//Get template properties, ie:  size="medium"
			angular.forEach(attrs.$attr, function(value, key) {
				data[value] = attrs[value];
			});
			Blaze.renderWithData(Template.ldrcAvatar, data, element[0]);
		}
	}
}).directive('ldrcAvatarEdit', function() {
	return {
		restrict: 'E',
		scope: true,
		link: function(scope, element, attrs) {
			var data = {};
			angular.forEach(attrs.$attr, function(value, key) {
				data[value] = attrs[value];
			});
			Blaze.renderWithData(Template.ldrcAvatarEdit, data, element[0]);
		}
	}
});