# How to use?
- You should have `angular` and `aldricus:avatar` packages in your project.
- Add a AngularJs dependency on your module:
```javascript
angular.module('myApp', ['...' , 'ldrc.avatar']);
```
- Use below element in your app:
```html
<ldrc-avatar-edit size="medium"></ldrc-avatar-edit>
<ldrc-avatar userid="{{ user._id }}" size="medium"></ldrc-avatar>
```